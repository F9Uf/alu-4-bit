`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2018 07:52:04 PM
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU(
    input [3:0] ALU_control,Ain,Bin,
    output reg [3:0] ACC_out,
    output V, Z, C, Clk
    );
    
    wire reg [3:0] ALU_out;
    
    always @ (*)
    begin
        case(ALU_control)
        4'b0000: ALU_out = 0; // load
        4'b0001: {C,ALU_out} = Ain + Bin; // add
        4'b0010: {C,ALU_out} = Ain - Bin; // subtracct
        4'b0011: ALU_out = 1; // store
        4'b0100: ALU_out = 0; // clear
        4'b0101: ALU_out = Ain & Bin; // and
        4'b0110: ALU_out = Ain | Bin;// or
        4'b0111: ALU_out = ~Ain; // not
        4'b10xx: ALU_out = Ain << 2; // shl
        4'b11xx: ALU_out = Ain >> 2; // shr
        endcase
        
    end
    assign Z = (ALU_out == 0)? 1:0; // if zero
    if (C == 0 && ALU_out[3] == 1)  // overflow if only if 01 (c,first bit of result)
    begin
        assign V = 1;
    end
endmodule
